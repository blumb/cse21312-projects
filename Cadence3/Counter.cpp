#include "Counter.h"

unsigned Counter::number = 0 ;

Counter::Counter ( ) : count (0), value (count), mode (BIN) { ++number; } ;

Counter::Counter ( const Counter& other ) : value (count), mode (BIN)
{ count = other.get_count(); ++number; }

Counter::Counter ( unsigned count ) : count (count), value (count), mode (BIN)
{ ++number; }

Counter::~Counter ( ) { --number; }

void Counter::set_count ( unsigned count ) { this->count = count; }

unsigned Counter::get_count ( void ) const { return count; }

void Counter::do_count ( ) {
  switch ( mode ) {
    case BIN: ++value; break;
    case BCD: unsigned temp = value, result = 0, carry = 1 ;
              for ( unsigned i=0; i<2*sizeof(value); ++i ) {
                unsigned digit = (temp + carry) & 0xF ;
                if ( carry = digit == 0xA ) digit = 0x0 ;
                result += (digit<<(i*4)) ;
                temp >>= 4 ;
              }
              value = result ;
              break;
  }
}

void Counter::set ( unsigned count ) { this->count = count; }

void Counter::set ( mode_t mode ) { this->mode = mode; }

void Counter::set ( unsigned count, mode_t mode )
{ this->count = count; this->mode = mode; }

Counter::mode_t Counter::get_mode ( void ) const { return mode; }

unsigned Counter::get_number ( void ) { return number; }

Counter& Counter::operator= ( const Counter& other ) {
  if ( &other != this ) { set ( other.get_count(), other.get_mode() ); }
  return *this;
}

Counter& Counter::operator= ( unsigned count )
{ this->count = count; return *this; }
