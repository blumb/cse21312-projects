/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

struct Author{

        std::string FirstName;
        std::string LastName;

        Author(std:: string FirstName, std::string LastName) : FirstName(FirstName), LastName(LastName) {}
        bool operator<(const Author& rhs) const{
            if (LastName < rhs.LastName)
                return true;
            else if (LastName == rhs.LastName){
                if (FirstName < rhs.FirstName)
                    return true;
            }
            return false;
        }
        bool operator==(const Author& rhs){
            if (LastName != rhs.FirstName)
                return false;
            else{
                if (FirstName != rhs.FirstName)
                    return false;
            }
            return true;
        }

        friend std:: ostream& operator<< (std:: ostream& outStream, const Author& printAuth);

};

std::ostream& operator<<(std::ostream& outStream, const Author& printAuth){
        outStream << printAuth.LastName << "," << printAuth.FirstName;

        return outStream;
}
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    AVLTree<Author> authorsAVL;
    Author Aardvark ("Anthony",  "Aardvark");
    Author Aardvark2 ("Gregory",  "Aardvark");
    Author BadPerson ("BadMean",  "Person");
    Author McDowell ("Gayle",  "McDowell");
    Author Main ("Michael", "Main");
    Author Savitch ("Walter", "Savitch");

    authorsAVL.insert(Aardvark);
    authorsAVL.insert(Aardvark2);
    authorsAVL.insert(BadPerson);
    authorsAVL.insert(McDowell);
    authorsAVL.insert(Main);
    authorsAVL.insert(Savitch);
    authorsAVL.printTree();

    authorsAVL.remove(BadPerson);
    authorsAVL.printTree();
    return 0;
}
